﻿using System.Text.Json;

async Task checkvalue()
{
    string url = "https://api.github.com/users/hadley/orgs";
    string path = @"C:\Santosh\Dotnet Core\jsoncoding\jsondata.json";

    HttpClient client = new HttpClient();
     client.DefaultRequestHeaders.Add("User-Agent", ".NET Foundation Repository Reporter");
      client.DefaultRequestHeaders.Accept.Clear();

    var stringTask = client.GetStringAsync(url);
    var msg = await stringTask;
    
    //Check if File Exsits or Not.
    if(File.Exists(path))
    {
        //Delete previous File.
        File.Delete(path);
        //Export data to json file. 
        using (TextWriter tw = new StreamWriter(path,true))
        {
            tw.WriteLine(msg);
            tw.Close();
        };
    }


    string jsonString = File.ReadAllText(path);
    if(!string.IsNullOrEmpty(jsonString))
    {
        Orgs[] orgList = JsonSerializer.Deserialize<Orgs[]>(jsonString);

        if(orgList != null)
        {
            Console.WriteLine("Min:" +orgList.Min( item => item.id ).ToString());

            Console.WriteLine("Max:" +orgList.Max( item => item.id ).ToString());
        }
    }
 
}

await checkvalue();

class Orgs
 {
     public string login { get; set; }
     public int id { get; set; }
     public string node_id { get; set; }
     public string url { get; set; }
     public string repos_url { get; set; }
     public string events_url { get; set; }
     public string hooks_url { get; set; }
     public string issues_url { get; set; }
     public string members_url { get; set; }
     public string public_members_url { get; set; }
     public string avatar_url { get; set; }
     public string description { get; set; }
    
 }